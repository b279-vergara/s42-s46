const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth')
const Product = require("../models/Product")

// Check if email already exist

// routes for checking if email already exists.

module.exports.registerUser = (reqBody) => {
    
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10)
    }) 


    return newUser.save().then((user, error) => {
        if(error) {
            return false;
        } else {
            return true;
        }
    })

};



module.exports.loginUser = (reqBody) => {

    return User.findOne({email : reqBody.email}).then(result => {
        if(result == null){
            return false
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if(isPasswordCorrect){
                return {access: auth.createAccessToken(result)}
            }else{
                return false
            }
        }
    })

}


module.exports.checkout = async (data) => {
    if(!data.isAdmin){
        let productTemp = [];
        let totalAmountTemp = 0;
        let isProductUpdated;
        
        
        data.products.forEach(singleProduct => {
            Product.findById(singleProduct.productId).then(product => { 
                productTemp.push(
                    {
                        productId : singleProduct.productId,
                        productName : product.name,
                        quantity : singleProduct.quantity
                    }
                )
                totalAmountTemp = totalAmountTemp + (product.price * singleProduct.quantity)
            }
            )   
            isProductUpdated = Product.findById(singleProduct.productId).then(product =>{

                product.userOrders.push({userId : data.userId})
                return product.save().then((product, error) => {
                    if(error){
                        return false;
                    } else {
                        return true;
                    }
                })
            })
        });


        let isUserUpdated = await User.findById(data.userId).then(user => {
                user.orderedProduct.push(
                    {

                        product : productTemp,
                        totalAmount : totalAmountTemp
                    }
                );            
                return user.save().then((user, error) => {
                    if(error){
                        return false;
                    }else{
                        return true;
                    }
                })
        })
        

        if(isUserUpdated && isProductUpdated){
            return true;
        }else{
            return "Something went wrong with your request. Please try again later!";
        }
    } else {
        return Promise.resolve("Admins can't checkout products")
    }
}


module.exports.getProfile = (reqBody) => {
    return User.findById(reqBody.id).then(result =>{
        
        if(result){
            result.password = "";
            result.orderedProduct = [];
            return result;
        } else {
            return false
        }
    })
};

module.exports.getOrders = (reqBody) => {
    return User.findById(reqBody.id).then(result =>{
        
        if(result){
            return result.orderedProduct;
        } else {
            return false
        }
    })
};

module.exports.getAllOrders = () => {
    return User.find({}).then(result =>{
        let orders = []
        result.forEach(user => {
            if (user.orderedProduct[0] !== undefined){
                orders.push({
                    userId : user._id, 
                    orders : user.orderedProduct
                });
            }
        });
        if(result){
            return orders;
        } else {
            return false
        }
    })
};


module.exports.setAdmin = (reqParams, reqBody, isAdmin) => {
    if(isAdmin){
        let updateUser = {
            isAdmin : reqBody.isAdmin
        }
        
        return User.findByIdAndUpdate(reqParams.userId, updateUser).then((User, error) => {
            if(error){
                return false;
            }else{
                return `User Admin Privileges set to ${reqBody.isAdmin}`;
            }
        })
    }else{
        return Promise.resolve("you not admin bishhh")
    }
}