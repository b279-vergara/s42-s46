const Product = require('../models/Product');

// Check if email already exist

// routes for checking if email already exists.

module.exports.addProduct = (data) => {
	if(data.role == 'seller') {
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price,
            quantity: data.product.quantity,
            sellerId: data.id,
            image: data.file
		})

		return newProduct.save().then((product, error) => {
			if(error){
				return error;
			}
			return true;
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	return Promise.resolve(false);
}

module.exports.getAllActive = () => {
    return Product.find({isActive : true}).then(result =>{
        return result;
    })

}

module.exports.getAllProducts = (data) => {
    if(data.role == 'seller'){
        return Product.find({sellerId : data.id }).then(result =>{
            return result;
        })
    } else {
        return Promise.resolve(false)
    }
}

module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then(result => {
        return result;
    })
}

module.exports.updateProduct = (reqParams, data) => {
    if(data.role == 'seller' || data.role == 'admin'){
        let updateProduct = {
            name : data.reqBody.name,
            description : data.reqBody.description,
            price : data.reqBody.price,
            quantity : data.reqBody.quantity,
            isActive : data.reqBody.isActive,
            image: data.file
        }
        
        return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error) => {
            if(error){
                return false;
            }else{
                return true;
            }
        })
    }else{
        return Promise.resolve(false)
    }
}

module.exports.archiveProduct = (reqParams, reqBody, isAdmin) => {
    if(isAdmin){
        let updateProduct = {
            isActive : reqBody.isActive
        }
        
        return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((course, error) => {
            if(error){
                return false;
            }else{
                return `Product status set to ${reqBody.isActive}`;
            }
        })
    }else{
        return Promise.resolve("you not admin bishhh")
    }
}