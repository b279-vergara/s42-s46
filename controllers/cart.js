const Cart = require('../models/Cart')

module.exports.addToCart = (data) => {
	if(data.role == 'user') {
		let newCart = new Cart({
            userId: data.id,
            productId: data.product.productId,
			productName: data.product.productName,
			price: data.product.price,
            quantity: data.product.quantity,
            subTotal: data.product.subTotal,
            image: data.product.image
		})

		return newCart.save().then((cart,error) => {
			if(error){
				return error;
			}
			return true;
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	return Promise.resolve(false);
}

module.exports.getCart = (id) => {
    
        return Cart.find({userId : id }).then(result =>{
            return result;
        })
    
}

module.exports.updateCart = (reqParams, reqBody) => {

        let updateCart = {
            quantity : reqBody.quantity,
            subTotal : reqBody.subTotal
        }
        
        return Cart.findByIdAndUpdate(reqParams.cartId, updateCart).then((product, error) => {
            if(error){
                return false;
            }else{
                return true;
            }
        })
}

module.exports.deleteItem = (reqParams) => {
    return Cart.findByIdAndDelete(reqParams.cartId).then((res, error) => {
        if(error){
            return false;
        }else{
            return true;
        }
    })
}