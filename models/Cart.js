const  mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
    userId : {
        type : String,
    },
    productId : {
        type : String,
    },
    productName : {
        type : String,
    },
    price : {
        type : Number,
    },
    quantity : {
        type : Number,
    }, 
    subTotal : {
        type : Number,
    },
    image : {
        type : String,
    },
})

module.exports = mongoose.model("Cart", cartSchema);