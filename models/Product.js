const  mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    name : {
        type : String,
        required : [true, "PRODUCT NAME is required!"]
    },
    description : {
        type : String,
        required : [true, "PRODUCT DESCRIPTION is required!"]
    },
    price : {
        type : Number,
        required : [true, "PRODUCT PRICE is required!"]
    },
    quantity : {
        type : Number,
        required : [true, "PRODUCT QUANTITY is required!"]
    }, 
    sellerId : {
        type : String,
        required : [true, "Seller ID is required!"]
    },
    isActive : {
        type : Boolean,
        default : true
    },
    createdOn: {
        type : Date,
        // The "new Date()" expression instantiates
        default : new Date()
    },
    image: String,
    userOrders : [
        {
            userId : {
                type : String,
                required : [true, "USERID is required!"]
            }
        }
    ]

})

module.exports = mongoose.model("Product", productSchema);