const  mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName : {
        type : String,
        required : [true, "FIRST NAME is required!"]
    },
    lastName : {
        type : String,
        required : [true, "LAST NAME is required!"]
    },
    email : {
        type : String,
        required : [true, "EMAIL is required!"]
    },
    password : {
        type : String,
        required : [true, "PASSWORD is required!"]
    },
    role : {
        type : String,
        default : 'user'
    },
    mobileNo : {
        type : String,
        required : [true, "MOBILE NO is required!"]
    },
    orderedProduct : [
        {
            product :[
                {
                    productId : {
                        type : String,
                        required : [true, "Product ID is required!"]
                    },
                    productName : {
                        type : String,
                        required : [true, "Product Name is required!"]
                    },
                    quantity: {
                        type : Number,
                        required : [true, "Quantity is required!"]
                    }
                }
            ],
            totalAmount : {
                type : Number,
                required : [true, "Total amount required"]
            },
            purchasedOn:{
                type: Date,
                default: new Date()
            }
        }
    ]

})

module.exports = mongoose.model("User", userSchema);