const express = require('express');
const multer = require("multer");
const path = require('path');
const router = express.Router();
const cartController = require("../controllers/cart.js");
const auth = require("../auth.js")


router.post("/", auth.verify, (req, res) =>{
    console.log(req.body)
    const data = {
		product: req.body,
        id: auth.decode(req.headers.authorization).id,
		role: auth.decode(req.headers.authorization).role
	}
    cartController.addToCart(data).then(resultFromController => res.send(resultFromController));
});

router.get("/", auth.verify, (req, res) => {
    const id = auth.decode(req.headers.authorization).id;
    cartController.getCart(id).then(resultFromController => res.send(resultFromController));
})

router.put("/:cartId", (req, res) => {
    cartController.updateCart(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

router.delete("/:cartId", (req, res) => {
    cartController.deleteItem(req.params).then(resultFromController => res.send(resultFromController))
})

// Allows us to export the "router" object that will be access in our index.js file
module.exports = router;