const express = require('express');
const multer = require("multer");
const path = require('path');
const router = express.Router();
const productController = require("../controllers/product.js");
const auth = require("../auth.js")


const storage = multer.diskStorage({
    destination: './uploads',
    filename: (req, file, cb) => {
      const fileName = file.fieldname + '-' + Date.now() + path.extname(file.originalname);
      cb(null, fileName);
    },
  });

const upload = multer({ storage });

express().use(express.static('public'));
router.post("/", auth.verify, upload.single("image"), (req, res) =>{
    const { filename } = req.file;
    const imagePath = path.join('uploads', filename);
    const data = {
        file: imagePath,
		product: req.body,
        id: auth.decode(req.headers.authorization).id,
		role: auth.decode(req.headers.authorization).role
	}
    productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

router.get("/", (req, res) => {
    productController.getAllActive().then(resultFromController => res.send(resultFromController));
})

router.get("/all", auth.verify, (req, res) => {
    const data = {
        role: auth.decode(req.headers.authorization).role,
        id: auth.decode(req.headers.authorization).id
    }
    productController.getAllProducts(data).then(resultFromController => res.send(resultFromController));
})

router.get("/:productId", (req, res) => {
    productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

router.put("/:productId", upload.single("image"), auth.verify, (req, res) => {
    const { filename } = req.file;
    const imagePath = path.join('uploads', filename);
    let data = {
        file: imagePath,
       role : auth.decode(req.headers.authorization).role,
       reqBody : req.body
    }
    productController.updateProduct(req.params, data).then(resultFromController => res.send(resultFromController));
})

router.put("/:productId/archive", auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin
    productController.archiveProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})



// Allows us to export the "router" object that will be access in our index.js file
module.exports = router;