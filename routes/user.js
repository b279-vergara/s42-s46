const express = require('express');
const router = express.Router();
const userController = require("../controllers/user.js");
const auth = require("../auth.js")


router.post("/register", (req, res) =>{
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req,res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/checkout", (req, res) => {

    let data = {
        userId : auth.decode(req.headers.authorization).id,
        isAdmin : auth.decode(req.headers.authorization).isAdmin,
        products : req.body
    }
    userController.checkout(data).then(resultFromController => res.send(resultFromController))
})

router.post("/details",auth.verify,  (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    userController.getProfile(userData).then(resultFromController => res.send(resultFromController))
});

router.post("/orders",auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    userController.getOrders(userData).then(resultFromController => res.send(resultFromController))
});

router.post("/allorders",auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    userController.getAllOrders(userData).then(resultFromController => res.send(resultFromController))
});

router.put("/:userId/setadmin", auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin
    userController.setAdmin(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})

// Allows us to export the "router" object that will be access in our index.js file
module.exports = router;