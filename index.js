const express = require("express");
const mongoose = require("mongoose");
const multer = require("multer");
const cors = require("cors");
const userRoutes = require("./routes/user.js");
const productRoutes = require("./routes/product.js");
const cartRoutes = require("./routes/cart.js");
const path = require('path');


const app = express();

mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.uesrzu4.mongodb.net/Capstone2_API?retryWrites=true&w=majority",
    {
        useNewUrlParser : true,
        useUnifiedTopology : true 
    });

mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas."));


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use(express.static('public'));
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use('/cart', cartRoutes)
app.use('/uploads', express.static('uploads'));



if(require.main === module){
    app.listen(process.env.PORT || 4000, () => console.log(`API is now online on port ${process.env.PORT || 4000}`));
}

module.exports = app;